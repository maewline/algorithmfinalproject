/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.algorithmfinalproject;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Arthi
 */
public class Reverse {

    public static void main(String[] args) {

        Scanner kb = new Scanner(System.in);
        int count = kb.nextInt(); //รับจำนวน input
        int[] A = new int[count]; //ประกาศ array
        for (int i = 0; i < count; i++) { //รับข้อมูลเข้า
            A[i] = kb.nextInt();
        }
        for (int i : reverse(A)) { //แสดงผล
            System.out.print(i + " ");
        }
    }

    private static int[] reverse(int A[]) {
        int temp = 0; //จัดเก็บค่าตัวเลขชั่วคราวเพื่อทำการสลับตำแหน่ง
        int index = 0; // เป็น index ที่เริ่มจาก 0 ถึง จำนวนครึ่งหนึ่งของ lenght A 
        for (int i = A.length - 1; i >= Math.floor(A.length / 2); i--) { //เป็น for loop ที่เริ่มจาก index สุดท้ายของ A วนถอยหลังกลับมาจนถึงจำนวนครึ่งนึงของ lenght A เพื่อทำการสลับค่ากับตำแหน่งตัวแปร index   
            temp = A[index];
            A[index] = A[i];
            A[i] = temp;
            index++; //เพิ่มตำแหน่งขึ้นไป 1 step เพื่อสลับตำแหน่งต่อไป
        }
        return A;
    }
}
