/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package com.mycompany.algorithmfinalproject;

import java.util.Scanner;

/**
 *
 * @author Arthi
 */
public class StringToDigit {

    public static void main(String[] args) {
        String input = (new Scanner(System.in)).next();
        System.out.println(StringToDigits(input));
    }

    private static int StringToDigits(String A) {
        int sum = 0; //ค่าผลลัพท์
        int valueOfPosition = 1; //เป็นค่าของตำแหน่งตัวเลข เริ่มตั้งแต่หลักหน่วย สิบ ร้อย พัน...  
        for (int i = A.length() - 1; i >= 0; i--) { //วน loop เริ่มตั้งแต่หลักหน่วย (ตำแหน่งท้ายสุดของ String A) จนถึงหลักที่มากที่สุด (ตำแหน่งแรกสุดของ String A)
            if (A.charAt(i) == '1') {
                sum += 1 * valueOfPosition;
            } else if (A.charAt(i) == '2') {
                sum += 2 * valueOfPosition;
            }else if (A.charAt(i) == '3') {
                sum += 3 * valueOfPosition;
            }else if (A.charAt(i) == '4') {
                sum += 4 * valueOfPosition;
            }else if (A.charAt(i) == '5') {
                sum += 5 * valueOfPosition;
            }else if (A.charAt(i) == '6') {
                sum += 6 * valueOfPosition;
            }else if (A.charAt(i) == '7') {
                sum += 7 * valueOfPosition;
            }else if (A.charAt(i) == '8') {
                sum += 8 * valueOfPosition;
            }else if (A.charAt(i) == '9') { 
                sum += 9 * valueOfPosition;
            }else if (i == 0 && A.charAt(i) == '-') { //เช็คเงื่อนไขตำแหน่งแรกสุดว่าเป็น input ที่เป็นจำนวนเต็มติดลบหรือไม่
                sum *= -1; // ทำคำตอบให้ติดลบ
            }valueOfPosition*=10; // เพิ่มค่าตำแหน่งของตัวเลขขึ้น (10,100,1000,...) ทุกๆครั้งที่มีการเลื่อนตำแหน่งของ i ใน for loop
        }
        return sum;
    }
}
